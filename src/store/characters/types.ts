export interface ICharactersData {
  info: ICharacterInfo;
  results: Array<ICharacter>;
}

export interface ICharactersError {
  error: Error
}

export interface ICharacter {
  name?: string;
  id?: number;
  status?: string;
  species?: string
  type?: string
  gender?: string
  origin?: any
  location?: any
  image?: string;
  episode?: Array<string>;
  url?: string;
  created?: string;
}

export interface ICharacterInfo {
  count?: number;
  pages?: number;
  next?: string;
  prev?: string;
}
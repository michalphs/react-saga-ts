import * as actions from './actions';
import { AnyAction } from 'redux';
import { ICharactersData, ICharacterInfo } from './types';

export const characters = (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case actions.GET_CHARACTERS_REQUESTED:
      return { ...state, isLoading: true };
    case actions.GET_CHARACTERS_DONE:
      return { ...state, isLoading: false, data: action.payload.results, info: action.payload.info };
    case actions.GET_CHARACTERS_FAILED:
      return { ...state, isLoading: false, isError: true }
    default:
      return state;
  }
};

const initialState: ICharacterState = {
  data: [],
  info: {},
  isLoading: null,
  isError: null,
}


export interface ICharacterState {
  data: Array<ICharactersData>;
  info: ICharacterInfo;
  isLoading: boolean | null;
  isError: boolean | null;
}
import { call, put, takeEvery } from 'redux-saga/effects'
import { fetchApi } from 'api/fetchApi';
import { characterApi } from './api';
import * as actions from './actions';
import { AnyAction } from 'redux';

function* getCharacters(action: AnyAction) {
  const params = { page: action.payload };
  const { response, error } = yield call(fetchApi, characterApi, params)
  if (response) {
    yield put(actions.getCharactersDone(response.data));
  }
  else {
    yield put(actions.getCharactersFailed(error));
  }
}

export const getCharactersSaga = takeEvery(actions.GET_CHARACTERS_REQUESTED, getCharacters)
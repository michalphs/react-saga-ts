import { ICharactersData, ICharactersError } from "./types";

export const GET_CHARACTERS_REQUESTED = 'GET_CHARACTERS_REQUESTED';
export const GET_CHARACTERS_DONE = 'GET_CHARACTERS_DONE';
export const GET_CHARACTERS_FAILED = 'GET_CHARACTERS_FAILED';

export function getCharactersRequested(page: number) {
  return {
    type: GET_CHARACTERS_REQUESTED,
    payload: page
  };
}

export function getCharactersDone(data: ICharactersData) {
  return {
    type: GET_CHARACTERS_DONE,
    payload: data
  };
}

export function getCharactersFailed(error: ICharactersError) {
  return {
    type: GET_CHARACTERS_FAILED,
    error: error
  };
}


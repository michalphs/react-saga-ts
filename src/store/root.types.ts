import { ICharacterState } from "./characters/reducer";

export interface IState {
  characters: ICharacterState
}
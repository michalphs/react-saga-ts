import axios from 'axios';

export const fetchApi = (api: string, params: any) => {
  return axios({
    method: 'get',
    params,
    url: api,
  })
    .then(response => ({ response }))
    .catch(error => ({ error }))
}
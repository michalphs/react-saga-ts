import { ICharacter } from "store/characters/types";

export interface IProps {
  character?: ICharacter;
}

import React from 'react'
import { IProps } from './CharacterItem.types';
import { Col, Card } from 'antd';
import { Meta } from 'antd/lib/list/Item';

const CharacterItem = ({ character }: IProps) => {
  if (!character) return null
  return (
    <Col className="gutter-row" sm={12} md={8}  xl={6}>
      <Card
        hoverable
        style={{margin: 10}}
        cover={<img alt="example" style={{ height: 300 }} src={character.image} />}
      >
        <Meta title={character.name} />
        <p>Status: {character.status}</p>
        <p>Gender: {character.gender}</p>
        <p>Species: {character.species}</p>
        <p>Origin: {character.origin.name}</p>
      </Card>
    </Col>
  )
}

export default CharacterItem

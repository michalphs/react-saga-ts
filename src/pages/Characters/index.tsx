import React from 'react'
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import * as actions from 'store/characters/actions'
import CharacterList from 'containers/CharacterList';
import { IProps } from './Characters.types';

class Characters extends React.PureComponent<IProps> {

  componentDidMount() {
    this.props.getCharacters()
  }

  render() {
    return <CharacterList />
  }
}
const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    getCharacters: (payload: number) => dispatch(actions.getCharactersRequested(payload)),
  }
};

export default connect(null, mapDispatchToProps)(Characters);

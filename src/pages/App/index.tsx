import React from 'react'
// @ts-ignore
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import Characters from 'pages/Characters';
import Home from 'pages/Home';
import { Layout, Menu } from 'antd';
import './App.css'
import CharacterDetails from 'pages/CharacterDetails';

const { Header, Content, Footer } = Layout;
const App = () => {
  return (
    <Router>
      <Layout style={{ backgroundColor: "#FFFFFF", minHeight: "100vh" }} >
        <Header >
          <Menu
            theme="dark"
            mode="horizontal"
            style={{ lineHeight: '64px', maxWidth: 1400, margin: '0 auto' }}
          >
            <Menu.Item key="home"><Link to="/">Home</Link></Menu.Item>
            <Menu.Item key="characters"><Link to="/characters">Characters</Link></Menu.Item>
          </Menu>
        </Header>
        <Content className="wrapper">
          <Route exact path="/" component={Home} />
          <Route path="/characters" component={Characters} />
          <Route path="/character/:id" component={CharacterDetails} />
        </Content>
        <Footer style={{ textAlign: 'center' }}>The Rick and Morty API </Footer>
      </Layout>
    </Router>
  )
}

export default App

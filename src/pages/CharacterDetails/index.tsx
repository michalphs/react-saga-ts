import React from 'react'
import { IState } from 'store/root.types';
import { connect } from 'react-redux';
import { Avatar, Card, Row, Col } from 'antd';
import Meta from 'antd/lib/card/Meta';

class CharacterDetails extends React.PureComponent<any>{
  getCharacter() {
    const { characters, match } = this.props;
    return characters.data.filter((character: any) => (character.id == match.params.id) ? character : null)
  }
  render() {
    const character = this.getCharacter()[0]
    if (!character) return null;
    console.log(character)
    return (
      <Row>
        <Col span="12">

        <Card
          style={{ width: 300 }}
          cover={
            <img
              src={character.image}
            />
          }
        >
          <Meta
            title={`${character.id} - ${character.name} (${character.gender})`}
            description={
              <div>
                created:  {character.created}<br />
                location: {character.location.name}<br />
                origin: {character.origin.name}<br />
                species:  {character.species}<br />
                status: {character.status}<br />
                type:   {character.type}<br />
                url:    {character.url}<br />
              </div>
            }
          />
        </Card>,
        </Col>

      </Row>
    )
  }
}
const mapStateToProps = (state: IState) => {
  return {
    characters: state.characters,
  }
};

export default connect(mapStateToProps, null)(CharacterDetails)

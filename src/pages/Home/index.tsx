import React from 'react'
import { Typography } from 'antd';
const { Title, Text } = Typography;

const Home = () => {
  return (
    <div>
      <Title style={{ marginTop: 50 }}>The Rick and Morty API</Title>
    </div>
  )
}

export default Home

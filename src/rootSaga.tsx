import { getCharactersSaga } from './store/characters/sagas';
import { all } from "redux-saga/effects";

export  function* rootSaga() {
  yield all([
    getCharactersSaga
  ])
}
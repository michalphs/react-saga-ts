import { ICharacterState } from "store/characters/reducer";

export interface IProps {
  characters: ICharacterState;
  getCharacters: Function
}
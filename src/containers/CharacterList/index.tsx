import React from 'react'
import { Dispatch } from 'redux';
import { Row, message, Pagination, Col, Typography } from 'antd';
import { IProps } from './CharacterListProps.types';
import { connect } from 'react-redux';
import CharacterItem from 'components/CharacterItem';
import * as actions from "store/characters/actions"
import { IState } from 'store/root.types';
// @ts-ignore
import { Link } from 'react-router-dom';
const { Title } = Typography;
class CharactersList extends React.PureComponent<IProps> {

  checkStatus() {
    const { characters } = this.props
    if (characters.isLoading === false && !characters.isError) { message.success("Fetch success") } 
    else if (characters.isError) { message.error("Fetch error") }
  }

  componentDidMount() {
    this.checkStatus()
  }

  componentDidUpdate() {
    this.checkStatus()
  }

  onChange = (page: number) => {
    this.props.getCharacters(page)
  }

  renderItems() {
    return this.props.characters.data.map((character: any) => {
      return (
        <Link key={character.id} to={`/character/${character.id}`}>
          <CharacterItem character={character}/>
        </Link>
      )
    })
  }

  render() {
    const { characters } = this.props;
    if (!characters.data) return null
    return (
      <Row>
        <Col span={24} style={{ textAlign: "center", margin: "50px 0" }}>
          <Title>Characters List</Title>
        </Col>
        {  this.renderItems()}
        <Col span={24} >
          <Pagination 
            style={{ display: 'flex', justifyContent: 'center', margin: "30px 0" }}
            onChange={this.onChange}
            pageSize={20}
            total={characters.info.count} 
            />
          </Col>
      </Row>
    )
  }
}

const mapStateToProps = (state: IState) => {
  return {
    characters: state.characters,
  }
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    getCharacters: (payload: number) => dispatch(actions.getCharactersRequested(payload)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(CharactersList);

